import java.util.*;

public class Main {

    public static void main(String[] args) {
        SkipList<Long> list= new SkipList<>();
        int lineNumber = 0;

        Scanner sc = new Scanner(System.in);
        try {
            while (lineNumber < 6002){
                String command = "";
                String input = "";
                String[] detect;
                long number = Integer.MAX_VALUE;
                input = sc.nextLine();
                lineNumber++;
                detect = input.split(" ");
                if (detect.length > 2 || detect.length == 0 ) {
                    continue;
                }
                command = detect[0].toLowerCase();
                try{
                    if (detect.length > 1){
                        number = Long.parseLong(detect[1]);
                    }
                }
                catch (NumberFormatException e){
                    continue;
                }


                switch (command){
                    case "insert":
                        list.insert(number);
                        break;
                    case "search":
                        list.search(number);
                        break;
                    case "delete":
                        list.delete(number);
                        break;
                    case "print":
                        list.print();
                        break;
                }
            }

        }
        catch (Exception e){

        }


    }
}

/**
 * this is a generic class that holds all functionality which is necessary for building the list
 * @param <T>
 */
class SkipList<T extends Comparable<T>>{
    public final static double PROBABILITY = 0.25;
    Random random = new Random();
    Node<T> head ;

    HashSet<T> set = new HashSet<>();
    public SkipList(){
        head = new Node<>(null , 0);
    }

    /**
     *  insert any key that given to list but it checks the key if it does not exist in the list other wise
     *  the key won't insert.
     *  for checking the existence of the key I used hashSet (Set is a data structure that all it's member is
     *  unique and search , print and insert complexity is O(1))
     * @param key
     */
    public void insert(T key){
        long level = addLevel();
        head = insertLevels(head , level);
        if (!set.contains(key)){
            head.insert(key , level , null);
        }
        set.add(key);

    }

    /**
     * build a list member with given level if the level is more than current level it adds another level
     * to the item of the list and move the pointer to the head of the item
     * @param node current node that pointer is on it
     * @param level given level that is calculated with probability
     * @return a node with given level
     */
    private Node insertLevels(Node node , long level){
        while (node.level < level){
            Node newNode = new Node(null , node.level+1);
            node.setUp(newNode);
            newNode.setDown(node);
            node = newNode;
        }

        return node;
    }

    /**
     * calculate level of each item with given probability
     * @return output level
     */
    private long addLevel(){
        long level = 0;
        while (random.nextDouble()<PROBABILITY && level < 10){
            level++;
        }
        return level;
    }

    /**
     * search for key and output the answer
     * @param key
     */

    public void search(T key){
        T result = null;
        try {
            result =  head.search(key).getData();
        }
        catch (NullPointerException e){
        }
        if (result != null){
            System.out.println(true);
        }
        else {
            System.out.println(false);
        }
    }

    /**
     * delete a node and fix the list by connecting the successor node and predecessor node together
     * and output error if the node does not exist in the list
     * @param key
     */
    public void delete(T key) {
        if (head.search(key) != null){
            for (Node<T> node = head.search(key); node != null; node = node.getDown()) {
                node.getPrev().setNext(node.getNext());
                if (node.getNext() != null) {
                    node.getNext().setPrev(node.getPrev());
                }
            }

        }
        else {
            System.out.println("error");
        }
    }

    /**
     * print all item in the list
     * move pointer to level 0 node of the list then move next and print the key
     */
    public void print(){
        String result = "";
        Node help = head;
        for (int i =0 ; i < head.level ; i++){
            help = help.getDown();
        }
        while (help.getNext() != null){
            help = help.getNext();
            result += help.getData() + " ";
        }
        if (result.trim().equals("")){
            result = "empty";
        }
        System.out.println(result);
    }
    @Override
    public String toString() {
        return head.toString();
    }
}

/**
 * base class of a single node that has 4 pointer to other nodes up,down,next and previous node
 * @param <T>
 */
 class Node<T extends  Comparable<T>> {
    private Node<T> up;
    private Node<T> down;
    private Node<T> prev;
    private Node<T> next;
    private T data;
    public long level;

    /**
     * the constructor of class that attach the key and the level of the node
     * @param data
     * @param level
     */
    public Node (T data , long level){
        this.data = data;
        this.level = level;
    }

    /**
     * search a key based on the given algorithm
     * compareResult  = 0 if (next.getData == key)
     * compareResult = 1 if (next.getData > key)
     * compareResult = -1 if (next.getData < key)
     * @param key
     * @return node if it can be found in the list other wise return null
     */

     public Node<T> search(T key) {
         if (next != null) {
             int compareResult = next.getData().compareTo(key);
             if (compareResult == 0) {
                 return next;
             } else if (compareResult < 0) {
                 return next.search(key);
             } else if (down != null) {
                 return down.search(key);
             } else {
                 return null;
             }
         } else if (down != null) {
             return down.search(key);
         } else {
             return null;
         }
     }

    /**
     * insert a node in the list with its level in right order because the list is sorted
     * @param key
     * @param level
     * @param parent
     */
    public void insert(T key , long level , Node parent){
        if (this.level <= level && (next == null || next.getData().compareTo(key) > 0)) {
            Node<T> newNode = new Node<>(key , this.level);
            if (next != null) {
                next.setPrev(newNode);
                newNode.setNext(next);
            }
            next = newNode;
            newNode.setPrev(this);
            if (parent != null) {
                newNode.setUp(parent);
                parent.setDown(newNode);
            }

            if (down != null) {
                down.insert(key, level, newNode);
            }
        } else if (next != null && next.getData().compareTo(key) < 0) {
            next.insert(key, level, parent);
        } else if (next != null && next.getData().compareTo(key) == 0) {
            return;
        } else if (down != null) {
            down.insert(key, level, parent);
        }
    }

     public Node<T> getUp() {
         return up;
     }

     public void setUp(Node<T> up) {
         this.up = up;
     }

     public Node<T> getDown() {
         return down;
     }

     public void setDown(Node<T> down) {
         this.down = down;
     }

     public Node<T> getPrev() {
         return prev;
     }

     public void setPrev(Node<T> prev) {
         this.prev = prev;
     }

     public Node<T> getNext() {
         return next;
     }

     public void setNext(Node<T> next) {
         this.next = next;
     }

     public T getData() {
         return data;
     }

     public void setData(T data) {
         this.data = data;
     }
 }


